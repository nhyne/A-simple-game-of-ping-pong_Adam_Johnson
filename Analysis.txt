Analysis

1)
When I first looked at the problem, I thought that Serena would win the most games because she had the most
spread out returning stats. However I soon realized this was wrong when I started running more games. When I ran
large numbers, I realized that Serena was the worst player out of the three playing. When I double checked each 
player's stats, I saw that Serena only had a 10% chance of returning Flat shots, which Jean and Bruce both hit the
most often, giving her a high probability of losing to the other two players.

The player to win the most games on average was pretty tough to determine. Jean and Bruce both had extremely
close numbers of wins. Checking their stats against each other, they both had good return stats against the 
other's most commonly hit shot. In order to figure out who won the most, I had each player play each other
100,000 times for a total of 300,000 games. After which, Bruce had won 119,237; Jean had won 118,527; and Serena 
had won 62,236. After how close these games were, I cut Serena out of the mix and had Bruce and Jean play each 
other for 200,000 games. 

Between just Jean and Bruce, Bruce wins the most games (Bruce: 120,885; Jean:79,115). I would say that between 
the three players Bruce wins the most games because while Jean is close, it is only because they win more games
over Serena than Bruce does.  


2)
I was able to confidently make this determination after I had had each player play each other 100,000 times and 
then after I had Bruce and Jean play each other 200,000 times for a total of 500,000 games.