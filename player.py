import random

class player:
    hit_flat=0
    hit_slice=0
    hit_topspin=0
    hit_unreturnable=0
    return_flat=0
    return_slice=0
    return_topspin=0
    name = ""

    #takes the player name, and their hitting and returning stats
    def __init__(self, name, hit_f, hit_s, hit_t, hit_u, r_f, r_s, r_t):
        self.hit_flat=hit_f
        self.hit_slice=hit_s
        self.hit_topspin=hit_t
        self.hit_unreturnable=hit_u
        self.return_flat=r_f
        self.return_slice=r_s
        self.return_topspin=r_t
        self.name = name

    #checks to see if the player can return the shot or not, returns a boolean
    def return_shot(self, shot):
        r_percent = random.randint(0,100)
        if str(shot) == "flat" and r_percent <= self.return_flat:
            return True
        elif str(shot) == "slice" and r_percent <= self.return_slice:
            return True
        elif str(shot) == "topspin" and r_percent <= self.return_topspin:
            return True
        else:
            return False

    #determines what kind of shot the player will hit, returns a string
    def shot_type(self):
        type = random.randint(0,100)
        if type <= self.hit_flat:
            return "flat"
        elif type <= self.hit_flat + self.hit_slice:
            return "slice"
        elif type <= self.hit_flat + self.hit_slice + self.hit_topspin:
            return "topspin"
        else:
            return "unreturnable"

    #returns the name of the player
    def return_name(self):
        return self.name
