from game import game
from player import player

#win_count is organized alphabetically
win_count = [0, 0, 0]

Bruce = player("Bruce", 47, 25, 25, 3, 80, 45, 75)
Jean = player("Jean", 70, 10, 15, 5, 90, 25, 85)
Serena = player("Serena", 10, 20, 66, 4, 65, 50, 85)

game_one = game(Serena, Jean)
game_two = game(Serena, Bruce)
game_three = game(Bruce, Jean)

#each player will play the other two players GAMES number of times
GAMES = 50

for x in range(0, GAMES):
	winner1 = game_one.play()
	winner2 = game_two.play()
	winner3 = game_three.play()

	if winner1 == "Bruce" : 
		win_count[0] += 1
	elif winner1 == "Jean" :
		win_count[1] += 1
	else:
		win_count[2] += 1

	if winner2 == "Bruce" : 
		win_count[0] += 1
	elif winner2 == "Jean" :
		win_count[1] += 1
	else:
		win_count[2] += 1

	if winner3 == "Bruce" : 
		win_count[0] += 1
	elif winner3 == "Jean" :
		win_count[1] += 1
	else:
		win_count[2] += 1




print win_count
