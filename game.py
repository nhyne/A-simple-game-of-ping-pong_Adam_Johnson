class game:
    player1 = 0
    player2 = 0
    # Bruce=0
    # Serena=0
    # Jean=0

    def __init__(self, player1, player2):
        self.player1 = player1
        self.player2 = player2

        #this method will play a game between the 
        #two given players until one score 21 points
        #the winner's name will then be returned
    def play(self): 
        #initial variable declaration
        P1_score = 0
        P2_score = 0
        serve_count = 0 #used to count how many times a player has served
        P1_serve = True #used to determine whose turn it is to serve
        P1_shot = False #used to determine whose turn it is to hit
        currentShot = ""
        #until one player has scored 21 points
        while max(P1_score, P2_score) < 21:
            #player 1 serves
            if P1_serve:
                currentShot = self.player1.shot_type()
                print self.player1.return_name() + " served a " + currentShot + " shot!"
                P1_shot = False 
                while currentShot != "null": #this loop runs until someone does not return a shot
                    if P1_shot:
                        if self.player1.return_shot(currentShot):
                            currentShot = self.player1.shot_type()
                            print self.player1.return_name() + " returned a " + currentShot + " shot!"
                            P1_shot = not P1_shot 
                        else:
                            print self.player1.return_name() + " failed to return"
                            P2_score += 1
                            if P2_score == 1:
                                print self.player2.return_name() + " scored, and now has " + str(P2_score) + " point!"
                            else:
                                print self.player2.return_name() + " scored, and now has " + str(P2_score) + " points!"
                            currentShot = "null"
                    elif not P1_shot:
                        if self.player2.return_shot(currentShot):
                            currentShot = self.player2.shot_type()
                            print self.player2.return_name() + " returned a " + currentShot + " shot!"
                            P1_shot = not P1_shot
                        else:
                            print self.player2.return_name() + " failed to return"
                            P1_score += 1
                            if P1_score == 1:  
                                print self.player1.return_name() + " scored, and now has " + str(P1_score) + " point!"
                            else:
                                print self.player1.return_name() + " scored, and now has " + str(P1_score) + " points!"
                            currentShot = "null"
                serve_count += 1

            else:
                currentShot = self.player2.shot_type()
                print self.player2.return_name() + " served a " + currentShot + " shot!"
                P1_shot = True
                while currentShot != "null": #this loop runs until someone does not return a shot
                    if P1_shot:
                        if self.player1.return_shot(currentShot):
                            currentShot = self.player1.shot_type()
                            print self.player1.return_name() + " returned a " + currentShot + " shot!"
                            P1_shot = not P1_shot
                        else:
                            print self.player1.return_name() + " failed to return"
                            P2_score += 1
                            if P2_score == 1:
                                print self.player2.return_name() + " scored, and now has " + str(P2_score) + " point!"
                            else:
                                print self.player2.return_name() + " scored, and now has " + str(P2_score) + " points!"
                            currentShot = "null"
                    elif not P1_shot:
                        if self.player2.return_shot(currentShot):
                            currentShot = self.player2.shot_type()
                            print self.player2.return_name() + " returned a " + currentShot + " shot!"
                            P1_shot = not P1_shot
                        else:
                            print self.player2.return_name() + " failed to return"
                            P1_score += 1
                            if P1_score == 1:  
                                print self.player1.return_name() + " scored, and now has " + str(P1_score) + " point!"
                            else:
                                print self.player1.return_name() + " scored, and now has " + str(P1_score) + " points!"
                            currentShot = "null"
                serve_count += 1

            if serve_count == 5: #when one player has served 5 times, the other player serves and the counter is reset
                P1_serve = not P1_serve
                serve_count = 0
        if P1_score > P2_score: #when one player has scored 21 points, returns that player's name
            return self.player1.return_name()
        else:
            return self.player2.return_name()
