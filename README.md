A-simple-game-of-ping-pong_Adam_Johnson
=======================================

The SevenRooms code challenge


This git is the code challenge for the application to work for SevenRooms. The challenge requires the creator to simulate a game of ping-pong between two players in a system of three players. The output is a series of human-readable play-by-play statements. 

The link to the original challenge: https://github.com/sevenrooms/sevenrooms_code_challenge
